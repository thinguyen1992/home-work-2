const readJsonSync = require('read-json-sync');
const Event = require('events');
class CountryReader extends Event {
    readData(filename) {
        let index = 0;
        // Choose readJsonSync because It automatically ignores the leading byte order mark.
        const countryJson = readJsonSync(filename);
        // Get how many object exist in file
        const objectCount = countryJson.length;
        // If index while read still less than object => Will call to print object 
        while(index < objectCount){
            setTimeout(()=> {
            var object = {};
            object.country = countryJson[index].country;
            object.index = index + 1;
            object.city = countryJson[index].city;
            this.emit('countrydata', object);
            index = index + 1;
            this.readData(filename);
            },1000);  

        }
    }
}
module.exports = CountryReader;